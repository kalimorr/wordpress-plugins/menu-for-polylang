<?php

namespace KrrMenuForPolylang;

/**
 * Class MenuRender
 *
 * @package KrrMenuForPolylang
 */
class MenuRender
{
	/**
	 * MenuRender constructor.
	 */
	public function __construct()
	{
		add_filter('wp_nav_menu_objects', [$this, 'editLinks'], 10, 2);
	}

	/**
	 * Edit the links data before the menu render
	 *
	 * @param $items
	 * @param $args
	 *
	 * @return mixed
	 */
	public function editLinks($items, $args)
	{
		/* If the current menu is not translated, do no modifications */
		if (!Plugin::isTranslatable($args->menu)) {
			return $items;
		}

		$locale = pll_current_language();
		foreach ($items as $key => $item) {
			/* Change the label if it is defined */
			if ($data = $this->updateData($item, 'title', $locale)) {
				$items[$key]->title = $data;
			}

			/* Get screens of routable post types available in nav menus */
			$screens = get_post_types([
				'show_in_nav_menus' => true
			]);

			/* Change the link target */
			if (in_array($item->object, $screens)) {
				$items[$key]->url = get_permalink(pll_get_post($item->object_id, $locale));
			} elseif ($item->type === 'taxonomy') {
				$termId = intval($item->object_id);

				if ($associateTermId = pll_get_term($termId, $locale)) {
					$termId = intval($associateTermId);
				}

				$items[$key]->url = get_term_link($termId, $item->object);
			} elseif ($item->type === 'custom') {
				if ($data = $this->updateData($item, 'url', $locale)) {
					$items[$key]->url = $data;
				}
			}
		}

		return $items;
	}

	/**
	 * Return the custom menu data if it is defined
	 *
	 * @param $item
	 * @param $type
	 * @param $locale
	 *
	 * @return mixed
	 */
	private function updateData($item, $type, $locale)
	{
		if (key_exists($type, $item->KRR_MFPLL)
			&& key_exists($locale, $item->KRR_MFPLL[$type])
			&& !empty($item->KRR_MFPLL[$type][$locale])) {
			return $item->KRR_MFPLL[$type][$locale];
		}

		return false;
	}
}