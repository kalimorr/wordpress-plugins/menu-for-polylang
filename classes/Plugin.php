<?php

namespace KrrMenuForPolylang;

/**
 * Class Plugin
 *
 * @package KrrMenuForPolylang
 */
class Plugin
{
	const BASE_OPTION_NAME = 'edit-menu-item-locale-';

	/**
	 * Plugin constructor.
	 */
	public function __construct()
	{
		add_action('plugins_loaded', [$this, 'load_i18n']);

		/* Check the plugin dependencies */
		$dependencies = new Dependencies();
		add_action('admin_init', [$dependencies, 'check']);

		add_action('admin_enqueue_scripts', [$this, 'enqueueScripts'], 11);

		/* Call the modules parts */
		new AdminMenu();
		new MenuRender();
	}

	/**
	 * Load plugin textdomain.
	 */
	public function load_i18n()
	{
		load_plugin_textdomain(
			'krr-mfpll',
			false,
			dirname(plugin_basename(KRR_MFPLL_FILE)) . '/languages'
		);
	}

	/**
	 * Add styles is current menu is translatable
	 */
	public function enqueueScripts()
	{
		if (AdminMenu::isCurrentTranslate()) {
			wp_enqueue_style(
				'krr-mfpll',
				plugin_dir_url(KRR_MFPLL_FILE) . 'css/style.css',
				[],
				false
			);
		}
	}

	/**
	 * Retrieve the option name saved in database
	 *
	 * @param string $type
	 * @param string $locale
	 *
	 * @return string
	 */
	public static function getBaseFieldName(string $type, string $locale)
	{
		return self::BASE_OPTION_NAME . $type . '-' . $locale;
	}

	/**
	 * Retrieve the field value of a specific locale
	 *
	 * @param $item_id
	 * @param $type
	 * @param $locale
	 *
	 * @return mixed
	 */
	public static function getFieldValue($item_id, $type, $locale)
	{
		$slug = Plugin::getBaseFieldName($type, $locale);
		return get_post_meta($item_id, $slug, true);
	}

	/**
	 * Check if the menu is translatable
	 *
	 * @param int|string|\WP_Term $menu
	 *
	 * @return bool
	 */
	public static function isTranslatable($menu)
	{
		/* Get the list of translated menus */
		$translatedMenus = get_option(AdminMenu::CONFIG_METAKEY);

		$menuSlug = wp_get_nav_menu_object($menu)->slug;

		/* If the current menu is not translated, do no modifications */
		return (is_array($translatedMenus) && in_array($menuSlug, $translatedMenus));
	}
}
