<?php

namespace KrrMenuForPolylang;

/**
 * Class Dependencies
 *
 * @package KrrMenuForPolylang
 */
class Dependencies
{

	/**
	 * Check for dependencies
	 */
	public function check()
	{
		if (!class_exists('Polylang')) {
			add_action('admin_notices', [$this, 'warning']);
			deactivate_plugins(plugin_basename(KRR_MFPLL_FILE));
		}
	}

	/**
	 * Notice the user that a dependency is missing
	 */
	public function warning()
	{
		$class = 'notice notice-error';
		$message = __('Menu For Polylang requires Polylang plugin. Please, install and activate it first.', 'krr-mfpll');

		printf( '<div class="%1$s"><p><strong>%2$s</strong></p></div>', esc_attr( $class ), esc_html( $message ) );
	}
}