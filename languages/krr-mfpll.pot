#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Menu For Polylang\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-04-06 18:36+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: \n"
"Language: \n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Loco https://localise.biz/\n"
"X-Loco-Version: 2.5.2; wp-5.7\n"
"X-Domain: krr-mfpll"

#. Author URI of the plugin
msgid "https://github.com/kofinorr"
msgstr ""

#. Author of the plugin
msgid "Kalimorr"
msgstr ""

#. Name of the plugin
msgid "Menu For Polylang"
msgstr ""

#: classes/Dependencies.php:30
msgid ""
"Menu For Polylang requires Polylang plugin. Please, install and activate it "
"first."
msgstr ""

#: views/adminMenuFields/label.php:15
msgid "Navigation Label"
msgstr ""

#. Description of the plugin
msgid "Simplify the translation of menus with Polylang."
msgstr ""

#: views/adminMenuFields/url.php:14
msgid "URL custom"
msgstr ""
