# Menu for Polylang
Simplify the translation of menus with Polylang.

## Requirements
* Require WordPress 4.7+ / Tested up to 5.7
* Require PHP 7.0
* Require Polylang 2.0+


## Installation

### Manual
- Download and install the plugin using the built-in WordPress plugin installer.
- No settings necessary, it's a Plug-and-Play plugin !

### Composer
- Add the following repository source : 
```
{
    "type": "vcs",
    "url": "https://gitlab.com/kalimorr/wordpress-plugins/menu-for-polylang.git"
}
```
- Include `"kalimorr/menu-for-polylang": "dev-master"` in your composer file for last master's commits or a tag released.
- No settings necessary, it's a Plug-and-Play plugin !

## Internationalisation
* English *(default)*
* Français

## License
"Menu for Polylang" is licensed under the GPLv3 or later.